package com.cartrust.atlas.ssikit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsiKitApplication {

	public static void main(String[] args) {
		SpringApplication.run(SsiKitApplication.class, args);
	}

}
