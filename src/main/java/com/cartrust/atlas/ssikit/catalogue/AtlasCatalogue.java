package com.cartrust.atlas.ssikit.catalogue;

import com.cartrust.atlas.ssikit.gx.AtlasServiceOfferingManager;
import com.cartrust.atlas.ssikit.waltid.AtlasSignatoryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.walt.credentials.w3c.VerifiableCredential;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public class AtlasCatalogue {
    private final AtlasSignatoryService signatoryService;
    private final AtlasCatalogueConfiguration atlasCatalogueConfiguration;
    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;
    private final Optional<AtlasServiceOfferingManager> atlasServiceOfferingManager;
    private boolean initialized;
    private Collection<VerifiableCredential> data;

    public AtlasCatalogue(
            AtlasSignatoryService signatoryService,
            AtlasCatalogueConfiguration atlasCatalogueConfiguration,
            RestTemplate restTemplate,
            ObjectMapper objectMapper,
            Optional<AtlasServiceOfferingManager> atlasServiceOfferingManager) {
        this.signatoryService = signatoryService;
        this.atlasCatalogueConfiguration = atlasCatalogueConfiguration;
        this.restTemplate = restTemplate;
        this.objectMapper = objectMapper;
        this.atlasServiceOfferingManager = atlasServiceOfferingManager;
    }

    public synchronized void init() {
        if (initialized) {
            return;
        }
        this.initialized = true;
        load();
    }

    public List<VerifiableCredential> findOne(String type) {
        Collection<VerifiableCredential> ref = data;
        return ref.stream().filter(vc -> vc.getType().contains(type)).toList();
    }

    public void load() {
        if (!signatoryService.isInitialized()) {
            log.debug("Signatory not initialized - please invoke setup identity api");
            return;
        }
        Map<String, VerifiableCredential> myServiceOfferings = atlasServiceOfferingManager
                .map(AtlasServiceOfferingManager::getAll)
                .orElse(new HashMap<>());
        List<CatalogueDto> catalogueDtos = atlasCatalogueConfiguration.getPeers()
                .stream()
                .map(this::queryPeer)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();

        Map<String, VerifiableCredential> all = myServiceOfferings.values().stream().collect(Collectors.toMap(VerifiableCredential::getId, e -> e));

        List<VerifiableCredential> remoteOfferings = catalogueDtos.stream()
                .flatMap(c -> c.getOfferings().stream())
                .map(this::writeSilent)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(this::fromString).toList();

        remoteOfferings.forEach(o -> all.computeIfAbsent(o.getId(), (id) -> o));
        Collection<VerifiableCredential> values = all.values();
        data = new ArrayList<>(values);
    }

    public CatalogueDto present() {
        CatalogueDto.CatalogueDtoBuilder builder = CatalogueDto.builder()
                .peers(atlasCatalogueConfiguration.getPeers());

        if (data != null) {
            data.stream()
                    .map(VerifiableCredential::encode)
                    .map(this::readSilent)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .forEach(builder::offering);
        }

        return builder.build();
    }

    private Object encodeToObject(VerifiableCredential vc) {
        String string = vc.encode();

        return null;
    }

    private Optional<CatalogueDto> queryPeer(String peer) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.ALL));
        String uri = UriComponentsBuilder.fromHttpUrl(peer)
                .encode()
                .toUriString();
        HttpEntity<String> httpEntity = new HttpEntity<>(headers);
        Map<String, Object> parameters = new HashMap<>();
        try {
            ResponseEntity<String> exchange = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, String.class, parameters);
            if (exchange.getStatusCode().is2xxSuccessful()) {
                String body = exchange.getBody();
                return Optional.ofNullable(objectMapper.readValue(body, CatalogueDto.class));
            }
        } catch (Exception e) {
            log.debug(e.getMessage());
        }
        return Optional.empty();
    }

    private Optional<String> writeSilent(Object o) {
        try {
            return Optional.ofNullable(objectMapper.writeValueAsString(o));
        } catch (JsonProcessingException e) {
            log.debug("JsonProcessingException: {}", e.getMessage());
            return Optional.empty();
        }
    }

    private Optional<Object> readSilent(String s) {
        try {
            return Optional.ofNullable(objectMapper.readValue(s, Object.class));
        } catch (JsonProcessingException e) {
            return Optional.empty();
        }
    }

    private VerifiableCredential fromString(String s) {
        return VerifiableCredential.Companion.fromString(s);
    }
}
