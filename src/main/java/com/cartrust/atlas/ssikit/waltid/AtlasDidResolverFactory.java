package com.cartrust.atlas.ssikit.waltid;

import id.walt.model.DidMethod;
import id.walt.services.did.composers.DidJwkDocumentComposer;
import id.walt.services.did.composers.DidKeyDocumentComposer;
import id.walt.services.did.resolvers.*;
import id.walt.services.key.KeyService;
import io.ktor.client.HttpClient;
import org.jetbrains.annotations.NotNull;

public class AtlasDidResolverFactory implements DidResolverFactoryInterface {

    private final HttpClient httpNoAuth;
    private final KeyService keyService;

    public AtlasDidResolverFactory(HttpClient httpNoAuth, KeyService keyService) {
        this.httpNoAuth = httpNoAuth;
        this.keyService = keyService;
    }

    @NotNull
    public DidResolver create(@NotNull String didMethod) {
        return switch (DidMethod.valueOf(didMethod)) {
            case key -> new DidKeyResolver(new DidKeyDocumentComposer(keyService));
            case web -> new DidWebResolver(httpNoAuth);
            case jwk -> new DidJwkResolver(new DidJwkDocumentComposer());
            case ebsi, iota, cheqd -> throw new RuntimeException("Not implemented");
        };
    }
}
