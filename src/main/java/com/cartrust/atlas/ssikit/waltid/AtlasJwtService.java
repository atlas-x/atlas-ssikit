package com.cartrust.atlas.ssikit.waltid;

import id.walt.sdjwt.JwtVerificationResult;
import id.walt.services.did.DidService;
import id.walt.services.jwt.WaltIdJwtService;
import id.walt.services.key.KeyService;
import org.jetbrains.annotations.NotNull;

public class AtlasJwtService extends WaltIdJwtService {
    private final KeyService keyService;
    private final DidService didService;

    public AtlasJwtService(KeyService keyService,
                           DidService didService) {
        this.keyService = keyService;
        this.didService = didService;
    }

    @NotNull
    @Override
    public KeyService getKeyService() {
        return keyService;
    }

    @Override
    public DidService getDidService() {
        return didService;
    }

    @Override
    public JwtVerificationResult verify(String token) {
        return super.verify(token);
    }
}
