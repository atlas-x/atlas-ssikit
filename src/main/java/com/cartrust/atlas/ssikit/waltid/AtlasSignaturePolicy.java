package com.cartrust.atlas.ssikit.waltid;

import id.walt.auditor.VerificationPolicy;
import id.walt.auditor.VerificationPolicyResult;
import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.services.vc.VerificationResult;
import id.walt.services.vc.WaltIdJsonLdCredentialService;
import id.walt.services.vc.WaltIdJwtCredentialService;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

@Slf4j
public class AtlasSignaturePolicy extends VerificationPolicy {
    private final WaltIdJsonLdCredentialService jsonLdCredentialService;
    private final WaltIdJwtCredentialService jwtCredentialService;

    public AtlasSignaturePolicy(
            WaltIdJsonLdCredentialService jsonLdCredentialService,
            WaltIdJwtCredentialService jwtCredentialService) {
        this.jsonLdCredentialService = jsonLdCredentialService;
        this.jwtCredentialService = jwtCredentialService;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Verify by signature";
    }

    @NotNull
    @Override
    protected VerificationPolicyResult doVerify(@NotNull VerifiableCredential vc) {
        try {
            log.debug("is jwt: {}", vc.getSdJwt() != null);
            VerificationResult verificationResult = verifyByFormatType(vc);
            return VerificationPolicyResult.Companion.success();
        } catch (Throwable e) {
            return VerificationPolicyResult.Companion.failure(e);
        }
    }

    private VerificationResult verifyByFormatType(VerifiableCredential vc) {
        String vcString = vc.encode();
        return Optional.ofNullable(vc.getSdJwt())
                .map(n -> jwtCredentialService.verify(vcString))
                .orElseGet(() -> jsonLdCredentialService.verify(vcString));
    }
}
