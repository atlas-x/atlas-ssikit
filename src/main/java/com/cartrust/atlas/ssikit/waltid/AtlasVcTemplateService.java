package com.cartrust.atlas.ssikit.waltid;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import id.walt.credentials.w3c.templates.VcTemplate;
import id.walt.credentials.w3c.templates.VcTemplateService;
import id.walt.services.hkvstore.HKVStoreService;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;

import java.time.Duration;

@Slf4j
public class AtlasVcTemplateService extends VcTemplateService
{
    private final String resourcePath = "/vc-templates";
    private final HKVStoreService hkvStore;
    private final Cache<String, VcTemplate> templateCache;

    private static final String SAVED_VC_TEMPLATES_KEY = "vc-templates";

    public AtlasVcTemplateService(HKVStoreService hkvStore) {
        this.hkvStore = hkvStore;
        templateCache = Caffeine.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(Duration.ofMinutes(10))
                .build();
    }

    @NotNull
    @Override
    public HKVStoreService getHkvStore() {
        return hkvStore;
    }

    @Override
    public Cache<String, VcTemplate> getTemplateCache() {
        return templateCache;
    }

    //    @Override
//    @NotNull
//    public VcTemplate register(@NotNull String name, VerifiableCredential template) {
//        W3CIssuer issuer = Optional.ofNullable(template.getIssuer()).map(it -> new W3CIssuer("", it.get_isObject(), it.getProperties())).orElse(null);
//        Optional.ofNullable(template.getCredentialSubject()).ifPresent(it -> it.setId(null));
//        template.setProof(null);
//        template.setIssuer(issuer);
//        template.setId(null);
//        hkvStore.put(new HKVKey(SAVED_VC_TEMPLATES_KEY, name), template.toJson());
//        return new VcTemplate(name, template, true);
//    }

//    @Override
//    public void unregisterTemplate(@NotNull String name) {
//        hkvStore.delete(new HKVKey(SAVED_VC_TEMPLATES_KEY, name), false);
//    }

//    @NotNull
//    @Override
//    public VcTemplate getTemplate(
//            @NotNull String name,
//            boolean populateTemplate,
//            @NotNull String runtimeTemplateFolder
//    ) {
//        VcTemplate cachedTemplate = retrieveOrLoadCachedTemplate(name, populateTemplate, runtimeTemplateFolder);
//        if (cachedTemplate.getTemplate() == null && populateTemplate) {
//            templateCache.invalidate(name);
//            return retrieveOrLoadCachedTemplate(name, true, runtimeTemplateFolder);
//        }
//        return cachedTemplate;
//    }

//
//    private List<String> listResources() {
//
//        ClassPathResource classPathResource = new ClassPathResource(resourcePath);
//        Assert.notNull(classPathResource, "Cannot find resource path: " + resourcePath);
//
//        log.debug("Loading templates from: {}", classPathResource);
//
//        File resource;
//        try {
//            resource = classPathResource.getFile();
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//        if (resource.isDirectory()) {
//            return Stream.of(Optional.ofNullable(resource.listFiles()).orElse(new File[0]))
//                    .filter(File::isFile)
//                    .filter(f -> f.getName().endsWith(".json"))
//                    .map(File::getName)
//                    .map(n -> n.substring(0, n.length() - 5))
//                    .toList();
//        }
//        return List.of();
//    }
//
//
//    private List<String> listRuntimeTemplates(String folderPath) {
//        File templatesFolder = new File(folderPath);
//        if (templatesFolder.isDirectory()) {
//            File[] s = Optional.ofNullable(templatesFolder.listFiles(File::isFile)).orElse(new File[0]);
//            return Arrays.stream(s).map(File::getName).toList();
//        } else {
//            log.debug("Requested runtime templates folder {} is not a folder.", folderPath);
//            return List.of();
//        }
//    }


//    @Override
//    @NotNull
//    public List<VcTemplate> listTemplates(@NotNull String runtimeTemplateFolder) { //"/vc-templates-runtime"
//        List<String> strings = listResources();
//        List<String> strings1 = hkvStore.listChildKeys(new HKVKey(SAVED_VC_TEMPLATES_KEY), false).stream().map(HKVKey::getName).toList();
//        List<String> strings2 = listRuntimeTemplates(runtimeTemplateFolder);
//        return Stream.of(strings, strings1, strings2).flatMap(Collection::stream)
//                .map(it -> getTemplate(it, false, runtimeTemplateFolder))
//                .toList();
//    }


//    private VcTemplate retrieveOrLoadCachedTemplate(
//            String name,
//            boolean populateTemplate, //true,
//            String runtimeTemplateFolder // "/vc-templates-runtime"
//    ) {
//        if (populateTemplate) {
//            // only add to cache, if template is populated
//            return templateCache.get(name, n -> loadTemplate(n, true, runtimeTemplateFolder));
//        } else {
//            // try to get from cache or load unpopulated template
//            VcTemplate vcTemplate = Optional.ofNullable(templateCache.getIfPresent(name))
//                    .orElseGet(() -> loadTemplate(name, false, runtimeTemplateFolder));
//            return new VcTemplate(vcTemplate.getName(), null, vcTemplate.getMutable());
//        }
//    }

//    private VcTemplate loadTemplate(String name, boolean populateTemplate, String runtimeTemplateFolder) {
//        VcTemplate vcTemplate = loadTemplateFromHkvStore(name, populateTemplate);
//        if (vcTemplate == null) {
//            vcTemplate = loadTemplateFromResources(name, populateTemplate);
//        }
//        if (vcTemplate == null) {
//            vcTemplate = loadTemplateFromFile(name, populateTemplate, runtimeTemplateFolder);
//        }
//        return Optional.ofNullable(vcTemplate)
//                .orElseThrow(() -> new IllegalArgumentException("No template found with name: "+name));
//    }

//    private VcTemplate loadTemplateFromFile(String name, boolean populateTemplate, String runtimeTemplateFolder) {
//        File file = new File(runtimeTemplateFolder + "/" + name + ".json");
//        return getFileContent(file)
//                .map(v -> stringToVcTemplate(v, name, populateTemplate, false))
//                .orElse(null);
//    }

//    private VcTemplate loadTemplateFromHkvStore(String name, boolean loadTemplate) {
//        return Optional.ofNullable(hkvStore.getAsString(new HKVKey(SAVED_VC_TEMPLATES_KEY, name)))
//                .map(vcString -> stringToVcTemplate(vcString, name, loadTemplate, true))
//                .orElse(null);
//    }

//    private VcTemplate loadTemplateFromResources(String name, boolean populateTemplate) {
//        ClassPathResource resource = new ClassPathResource(resourcePath + "/" + name + ".json");
//        if (resource.exists()) {
//            try {
//                return getFileContent(resource.getFile())
//                        .map(v -> stringToVcTemplate(v, name, populateTemplate, false))
//                        .orElse(null);
//            } catch (IOException e) {
//                throw new RuntimeException(e);
//            }
//        }
//        return null;
//    }

//    private Optional<String> getFileContent(File file) {
//        if (!file.exists()) {
//            return Optional.empty();
//        }
//        try {
//            return Optional.ofNullable(FileUtils.readFileToString(file, StandardCharsets.UTF_8));
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//    }

//    private static VcTemplate stringToVcTemplate(String input, String name, boolean populateTemplate, boolean isMutable) {
//
//        VerifiableCredential vc = populateTemplate && StringUtils.isNotBlank(input)
//                ? AtlasSignatoryService.toVerifiableCredential(input)
//                : null;
//        return new VcTemplate(name, vc, isMutable);
//    }
}
