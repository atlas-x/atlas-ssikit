package com.cartrust.atlas.ssikit.waltid;

import id.walt.services.did.DidService;
import id.walt.services.keystore.KeyStoreService;
import id.walt.services.vc.WaltIdJsonLdCredentialService;
import org.jetbrains.annotations.NotNull;

public class AtlasJsonLdCredentialService extends WaltIdJsonLdCredentialService {
    private final AtlasKeystoreService keystoreService;
    private final DidService didService;

    public AtlasJsonLdCredentialService(
            AtlasKeystoreService keystoreService,
            DidService didService) {
        this.keystoreService = keystoreService;
        this.didService = didService;
    }

    @NotNull
    @Override
    public DidService getDidService() {
        return didService;
    }

    @NotNull
    @Override
    public KeyStoreService getKeyStore() {
        return keystoreService;
    }
}
