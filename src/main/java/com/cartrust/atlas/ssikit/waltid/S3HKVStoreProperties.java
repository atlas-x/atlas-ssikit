package com.cartrust.atlas.ssikit.waltid;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class S3HKVStoreProperties {
    private String bucket;
    private String endpoint;
    private String region;
}
