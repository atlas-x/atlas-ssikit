package com.cartrust.atlas.ssikit.waltid;

import id.walt.services.hkvstore.HKVStoreService;
import id.walt.services.vcstore.HKVVcStoreService;
import org.jetbrains.annotations.NotNull;

public class AtlasHKVVcStoreService extends HKVVcStoreService {
    private final HKVStoreService hkvStoreService;

    public AtlasHKVVcStoreService(HKVStoreService hkvStoreService) {
        this.hkvStoreService = hkvStoreService;
    }

    @NotNull
    @Override
    public HKVStoreService getHkvStore() {
        return hkvStoreService;
    }
}
