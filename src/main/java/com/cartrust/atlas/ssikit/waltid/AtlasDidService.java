package com.cartrust.atlas.ssikit.waltid;

import com.cartrust.atlas.ssikit.waltid.AtlasCryptoService;
import com.cartrust.atlas.ssikit.waltid.AtlasDidResolverFactory;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import id.walt.model.Did;
import id.walt.model.DidUrl;
import id.walt.services.crypto.CryptoService;
import id.walt.services.did.DidService;
import id.walt.services.did.resolvers.DidResolverFactoryInterface;
import id.walt.services.hkvstore.HKVStoreService;
import id.walt.services.key.KeyService;
import id.walt.services.keystore.KeyStoreService;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;

import java.time.Duration;

@Slf4j
public class AtlasDidService extends DidService {

    private final CryptoService cryptoService;
    private final KeyService keyService;
    private final Cache<DidUrl, Did> didCache;
    private final HKVStoreService hkvStore;
    private final AtlasDidResolverFactory didResolverFactory;
    private final AtlasKeystoreService keystoreService;

    public AtlasDidService(AtlasCryptoService cryptoService,
                           AtlasKeyService atlasKeyService,
                           HKVStoreService hkvStore,
                           AtlasDidResolverFactory didResolverFactory,
                           AtlasKeystoreService keystoreService) {
        this.keyService = atlasKeyService;
        this.hkvStore = hkvStore;


        this.didResolverFactory = didResolverFactory;
        this.didCache = Caffeine.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(Duration.ofMinutes(10))
                .build();
        this.cryptoService = cryptoService;

        this.keystoreService = keystoreService;
    }

    @NotNull
    @Override
    public CryptoService getCryptoService() {
        return cryptoService;
    }

    @NotNull
    @Override
    public Cache<DidUrl, Did> getDidCache() {
        return didCache;
    }

    @NotNull
    @Override
    public DidResolverFactoryInterface getDidResolverFactory() {
        return didResolverFactory;
    }

    @NotNull
    @Override
    public KeyService getKeyService() {
        return keyService;
    }

    @NotNull
    @Override
    public HKVStoreService getHkvStore() {
        return hkvStore;
    }

    @NotNull
    @Override
    public KeyStoreService getKeyStore() {
        return keystoreService;
    }

//    @NotNull
//    @Override
//    public CheqdService getCheqdService() {
//        return null;
//    }
//
//    @NotNull
//    @Override
//    public JsonLdCredentialService getCredentialService() {
//        return null;
//    }
//
//    @NotNull
//    @Override
//    public IotaService getIotaService() {
//        return null;
//    }
//
//    @NotNull
//    @Override
//    public Did load(@NotNull String s) {
//        return null;
//    }
}
