package com.cartrust.atlas.ssikit.waltid;

import com.cartrust.atlas.ssikit.config.AtlasConfiguration;
import com.cartrust.atlas.ssikit.waltid.AtlasDidService;
import com.cartrust.atlas.ssikit.waltid.AtlasKeyService;
import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.credentials.w3c.VerifiablePresentation;
import id.walt.credentials.w3c.templates.VcTemplateService;
import id.walt.crypto.Key;
import id.walt.crypto.KeyAlgorithm;
import id.walt.crypto.KeyId;
import id.walt.model.Did;
import id.walt.model.DidMethod;
import id.walt.services.did.DidService;
import id.walt.services.did.DidWebCreateOptions;
import id.walt.services.vc.JsonLdCredentialService;
import id.walt.services.vc.JwtCredentialService;
import id.walt.services.vcstore.VcStoreService;
import id.walt.signatory.*;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class AtlasSignatoryService extends WaltIdSignatory {
    private final AtlasConfiguration configuration;
    private final AtlasKeyService atlasKeyService;
    private final AtlasDidService atlasDidService;
    private final AtlasVcTemplateService templateService;
    private final VcStoreService vcStore;
    private final JsonLdCredentialService jsonLdCredentialService;
    private final JwtCredentialService jwtCredentialService;
    private ProofConfig proofConfig;
    private boolean initialized = false;


    public AtlasSignatoryService(
            AtlasConfiguration configuration,
            AtlasKeyService atlasKeyService,
            AtlasDidService atlasDidService,
            AtlasVcTemplateService templateService,
            VcStoreService vcStore,
            JsonLdCredentialService jsonLdCredentialService,
            JwtCredentialService jwtCredentialService) {
        super(configuration.getSignatoryConfigPath());
        this.configuration = configuration;
        this.templateService = templateService;
        this.vcStore = vcStore;
        this.atlasKeyService = atlasKeyService;
        this.atlasDidService = atlasDidService;
        this.jsonLdCredentialService = jsonLdCredentialService;
        this.jwtCredentialService = jwtCredentialService;
    }

    public boolean isInitialized() {
        return initialized;
    }

    @PostConstruct
    void setup() {
        setup(false);
    }

    private void setup(boolean createIfMissing) {
        final String keyAlias = configuration.getSignatoryKeyAlias();
        String domain = configuration.getDomain();
        String nonce = configuration.getNonce();
        String path = configuration.getDidWebPath();

        KeyId serviceKeyId = null;

        try {
            Key serviceKey = atlasKeyService.load(keyAlias);
            serviceKeyId = serviceKey.getKeyId();
        } catch (Exception e) {
            if (createIfMissing) {
                serviceKeyId = atlasKeyService.generate(KeyAlgorithm.EdDSA_Ed25519);
                Key serviceKey = atlasKeyService.load(serviceKeyId.getId());
                atlasKeyService.addAlias(serviceKeyId, keyAlias);

                String didPath = String.join("/", Arrays.asList(configuration.getDidWebPath(), serviceKeyId.getId()));

                atlasDidService.create(DidMethod.web, serviceKeyId.getId(), new DidWebCreateOptions(domain, didPath));
            }
        }

        if (serviceKeyId == null) {
            return;
        }

        String didPath = Optional.ofNullable(path).map(p -> p.replaceAll("/", ":")).orElse(null);
        String issuerDid = Stream.of("did:web", domain, didPath, serviceKeyId.getId())
                .filter(StringUtils::isNotBlank)
                .collect(Collectors.joining(":"));

        Did didDoc = atlasDidService.load(issuerDid);

        log.info("\nDID WEB Document loaded:\n" + didDoc.encodePretty());

        proofConfig = Objects.requireNonNull(didDoc.getVerificationMethod()).stream().findFirst().map(issuerVerificationMethod -> new ProofConfig(
                issuerDid,
                issuerVerificationMethod.getId(),
                domain,
                nonce,
                ProofType.JWT,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                Ecosystem.DEFAULT,
                null,
                "",
                null,
                null
        )).orElseThrow(() -> new RuntimeException("No verification method found in didDoc"));

        initialized = true;
    }

    public synchronized void init() {
        if (initialized) {
            return;
        }
        setup(true);
    }

    public String getIdentity() {
        String subjectId;
        try {
            subjectId = getIssuerDid().get(100, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            throw new RuntimeException(e);
        }
        return subjectId;
    }

    public Future<String> getIssuerDid() {
        return new Future<>() {
            @Override
            public boolean cancel(boolean mayInterruptIfRunning) {
                return false;
            }

            @Override
            public boolean isCancelled() {
                return false;
            }

            @Override
            public boolean isDone() {
                return proofConfig != null;
            }

            @Override
            public String get() throws InterruptedException, ExecutionException {
                return proofConfig.getIssuerDid();
            }

            @Override
            public String get(long timeout, @NotNull TimeUnit unit) throws InterruptedException, TimeoutException {
                long c = 0;
                long total = unit.toMillis(timeout);
                while(c < total && proofConfig == null) {
                    c += 10;
                    //noinspection BusyWait
                    Thread.sleep(10);
                }
                if (proofConfig == null) {
                    throw new TimeoutException();
                }

                return proofConfig.getIssuerDid();
            }
        };
    }

    @NotNull
    @Override
    public SignatoryConfig getConfiguration() {
        return new SignatoryConfig(
                proofConfig,
                configuration.getTemplatesFolder()
        );
    }

    @NotNull
    @Override
    public VcStoreService getVcStore() {
        return vcStore;
    }

    @NotNull
    @Override
    public JsonLdCredentialService getJsonLdCredentialService() {
        return this.jsonLdCredentialService;
    }

    @NotNull
    @Override
    public JwtCredentialService getJwtCredentialService() {
        return jwtCredentialService;
    }

    @NotNull
    @Override
    public VcTemplateService getTemplateService() {
        return templateService;
    }

    @NotNull
    public DidService getDidService() {
        return atlasDidService;
    }

    public AtlasKeyService getAtlasKeyService() {
        return atlasKeyService;
    }

    public static VerifiableCredential toVerifiableCredential(String input) {
        VerifiableCredential vc = VerifiableCredential.Companion.fromString(input);
        return vc.getType().contains("VerifiablePresentation")
                ? VerifiablePresentation.Companion.fromVerifiableCredential(vc)
                : vc;
    }
}
