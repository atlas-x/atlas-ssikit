package com.cartrust.atlas.ssikit.waltid;

import id.walt.services.jwt.JwtService;
import id.walt.services.vc.WaltIdJwtCredentialService;
import org.jetbrains.annotations.NotNull;

public class AtlasJwtCredentialService extends WaltIdJwtCredentialService {
    private final JwtService jwtService;

    public AtlasJwtCredentialService(JwtService jwtService) {
        this.jwtService = jwtService;
    }

    @NotNull
    @Override
    public JwtService getJwtService() {
        return jwtService;
    }
}
