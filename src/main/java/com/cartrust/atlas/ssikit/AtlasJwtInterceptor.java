package com.cartrust.atlas.ssikit;


import com.cartrust.atlas.ssikit.config.AtlasJwtAuthorizationConfigurer;
import com.cartrust.atlas.ssikit.gx.GxCredentialSignedByAuthorityVerificationPolicy;
import com.cartrust.atlas.ssikit.waltid.AtlasSignaturePolicy;
import id.walt.auditor.VerificationPolicy;
import id.walt.auditor.VerificationResult;
import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.credentials.w3c.VerifiablePresentation;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.HandlerInterceptor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Slf4j
public class AtlasJwtInterceptor implements HandlerInterceptor {
    private final AtlasAuditor auditor;
    private final VerificationPolicy signatureValidator;
    private final AtlasJwtAuthorizationConfigurer configurer;

    public AtlasJwtInterceptor(
            AtlasAuditor auditor,
            AtlasSignaturePolicy signatureValidator,
            AtlasJwtAuthorizationConfigurer configurer) {
        this.auditor = auditor;
        this.signatureValidator = signatureValidator;
        this.configurer = configurer;
    }

    @Override
    public boolean preHandle(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler) throws Exception {
        boolean authorizationRequired = isAuthorizationRequired(request.getServletPath(), request.getMethod());
        if (!authorizationRequired) {
            return true;
        }
        String auth = request.getHeader("Authorization");
        if (auth == null) {
            log.debug("Authorization header missing");
            response.sendError(HttpStatus.UNAUTHORIZED.value(), "Authorization header missing");
            return false;
        }

        String vcString = auth.trim().startsWith("Bearer") ? auth.trim().substring("Bearer".length()).trim() : auth.trim();
        try {
            log.info("Authentication Header: {}", auth);
            VerifiableCredential authVc = VerifiableCredential.Companion.fromString(vcString);
            if (authVc.getType().contains("VerifiablePresentation")) {
                authVc = VerifiablePresentation.Companion.fromString(vcString);
            }
            VerificationResult verificationResult = checkVC(authVc, request.getServletPath(), request.getMethod());
            if (verificationResult.getResult()) {
                log.debug("JWT Authentication successful");
                return true;
            }
            verificationResult
                    .getPolicyResults()
                    .entrySet()
                    .stream()
                    .filter(e -> !e.getValue().isSuccess())
                    .findFirst()
                    .ifPresent(e -> {
                        log.debug(
                                "JWT failed: [{}] {}",
                                e.getKey(),
                                e.getValue()
                        );
                        try {
                            response.sendError(HttpStatus.UNAUTHORIZED.value(), e.getValue().toString());
                        } catch (IOException ex) {
                            throw new RuntimeException(ex);
                        }
                    });

            return false;
        } catch (Exception e) {
            log.debug("JWT failed due to exception: {}", e.getMessage());
            response.sendError(HttpStatus.UNAUTHORIZED.value(), e.getMessage());
            return false;
        }
    }

    private boolean isAuthorizationRequired(String path, String method) {
        return configurer.authorizationRequired(path, method);
    }

    private VerificationResult checkVC(VerifiableCredential vc, String path, String method) {
        List<VerificationPolicy> defaultPolicies = List.of(
                signatureValidator,
                new GxCredentialSignedByAuthorityVerificationPolicy()
//                new IssuedDateBeforePolicy(),
//                new ValidFromBeforePolicy(),
//                new ExpirationDateAfterPolicy()
        );
        List<VerificationPolicy> requestPolicies = configurer.policies(path, method);

        ArrayList<VerificationPolicy> policies = new ArrayList<>();
        policies.addAll(defaultPolicies);
        if (requestPolicies != null) {
            policies.addAll(requestPolicies);
        }
        return auditor.verify(vc, new HashSet<>(policies).stream().toList());
    }
}
