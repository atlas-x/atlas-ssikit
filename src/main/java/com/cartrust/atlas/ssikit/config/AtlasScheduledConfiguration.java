package com.cartrust.atlas.ssikit.config;

import com.cartrust.atlas.ssikit.catalogue.AtlasCatalogue;
import com.cartrust.atlas.ssikit.gx.AtlasSelfDescriptionManager;
import com.cartrust.atlas.ssikit.waltid.AtlasSignatoryService;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.PeriodicTrigger;

import java.time.Duration;
import java.util.Optional;

@Slf4j
@Configuration
public class AtlasScheduledConfiguration {
    private final AtlasConfiguration configuration;
    private final Optional<TaskScheduler> taskScheduler;

    private final AtlasSignatoryService signatoryService;
    private final Optional<AtlasSelfDescriptionManager> selfDescriptionManager;
    private final Optional<AtlasCatalogue> atlasCatalogue;

    private boolean sdSchedulerStarted = false;
    private boolean catalogueSchedulerStarted = false;

    public AtlasScheduledConfiguration(
            AtlasConfiguration configuration,
            Optional<TaskScheduler> taskScheduler,
            AtlasSignatoryService signatoryService,
            Optional<AtlasSelfDescriptionManager> selfDescriptionManager,
            Optional<AtlasCatalogue> atlasCatalogue) {
        this.configuration = configuration;
        this.taskScheduler = taskScheduler;
        this.signatoryService = signatoryService;
        this.selfDescriptionManager = selfDescriptionManager;
        this.atlasCatalogue = atlasCatalogue;
    }

    @PostConstruct
    void setup() {
        if (configuration.isAutostartSelfDescriptionSchedule()) {
            startScheduledTask();
        }
        startCatalogueScheduledTask();
    }

    public void startScheduledTask() {
        if (!signatoryService.isInitialized()) {
            throw new RuntimeException("Signatory not initialized - please invoke setup identity api");
        }
        if (sdSchedulerStarted) {
            log.info("Already started");
            return;
        }
        if (taskScheduler.isEmpty()) {
            throw new RuntimeException("No scheduler configured");
        }
        if (selfDescriptionManager.isEmpty()) {
            throw new RuntimeException("No self description manager configured");
        }
        sdSchedulerStarted = true;
        AtlasSelfDescriptionManager sdm = selfDescriptionManager.get();
        TaskScheduler ts = taskScheduler.get();
        PeriodicTrigger pT = new PeriodicTrigger(Duration.ofSeconds(configuration.getSelfDescriptionRefreshRate()));
        ts.schedule(sdm::check, pT);

    }

    public void startCatalogueScheduledTask() {
        if (catalogueSchedulerStarted) {
            log.info("Already started");
            return;
        }
        if (taskScheduler.isEmpty()) {
            log.debug("No scheduler configured");
            return;
        }
        if (atlasCatalogue.isEmpty()) {
            log.debug("No self description manager configured");
            return;
        }
        catalogueSchedulerStarted = true;
        AtlasCatalogue catalogue = atlasCatalogue.get();
        TaskScheduler ts = taskScheduler.get();
        PeriodicTrigger pT = new PeriodicTrigger(Duration.ofSeconds(configuration.getCatalogueRefreshRate()));
        ts.schedule(catalogue::load, pT);
    }
}
