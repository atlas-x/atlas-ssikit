package com.cartrust.atlas.ssikit.config;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AtlasConfiguration {
    @Builder.Default
    private String vcGroup = "default";

    @Builder.Default
    private String signatoryConfigPath = "/vc-templates";

    @Builder.Default
    private String signatoryKeyAlias = "signatory";

    @Builder.Default
    private String domain = "atlas.cartrust.com";

    @Builder.Default
    private String didWebPath = "gx/did";

    @Builder.Default
    private String nonce = "todo";

    @Builder.Default
    private String templatesFolder = "/vc-templates-runtime";

    @Builder.Default
    private String selfDescriptionTemplate = "GaiaxCredentialSD";

    @Builder.Default
    private String selfDescriptionVcId = "sd";

    @Builder.Default
    private String authorityVcAddress = "https://authority.atlas.cartrust.com/vc";

    @Builder.Default
    private String serviceBaseAddress = "https://example.com";

    @Builder.Default
    private String apiDocsBasePath = "/v3/api-docs";

    private boolean selfSignSelfDescription;

    @Builder.Default
    private boolean returnEmptySelfDescription = true;

    @Builder.Default
    private boolean autostartSelfDescriptionSchedule = false;

    @Builder.Default
    private int selfDescriptionRefreshRate = 10;

    @Builder.Default
    private int catalogueRefreshRate = 60;
}
