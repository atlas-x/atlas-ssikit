package com.cartrust.atlas.ssikit.config;

import com.cartrust.atlas.ssikit.AtlasAuditor;
import com.cartrust.atlas.ssikit.AtlasJwtInterceptor;
import com.cartrust.atlas.ssikit.catalogue.AtlasCatalogue;
import com.cartrust.atlas.ssikit.catalogue.AtlasCatalogueConfiguration;
import com.cartrust.atlas.ssikit.gx.AtlasSelfDescriptionSubjectBuilder;
import com.cartrust.atlas.ssikit.gx.AtlasServiceOfferingManager;
import com.cartrust.atlas.ssikit.gx.GxCredentialRoleVerificationPolicy;
import com.cartrust.atlas.ssikit.waltid.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.walt.services.WaltIdServices;
import id.walt.services.did.DidService;
import id.walt.services.hkvstore.FileSystemHKVStore;
import id.walt.services.hkvstore.HKVStoreService;
import id.walt.services.jwt.JwtService;
import id.walt.services.key.KeyService;
import id.walt.services.keystore.KeyStoreService;
import id.walt.services.vc.JsonLdCredentialService;
import id.walt.services.vc.JwtCredentialService;
import id.walt.services.vc.WaltIdJsonLdCredentialService;
import id.walt.services.vc.WaltIdJwtCredentialService;
import id.walt.services.vcstore.VcStoreService;
import lombok.SneakyThrows;
import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManagerBuilder;
import org.apache.hc.client5.http.io.HttpClientConnectionManager;
import org.apache.hc.client5.http.ssl.NoopHostnameVerifier;
import org.apache.hc.client5.http.ssl.SSLConnectionSocketFactory;
import org.apache.hc.core5.ssl.SSLContexts;
import org.apache.hc.core5.ssl.TrustStrategy;
import org.springdoc.core.models.GroupedOpenApi;
import org.springdoc.core.properties.SpringDocConfigProperties;
import org.springdoc.core.providers.SpringDocProviders;
import org.springdoc.core.service.AbstractRequestService;
import org.springdoc.core.service.GenericResponseService;
import org.springdoc.core.service.OpenAPIService;
import org.springdoc.core.service.OperationService;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Configuration
public class AtlasAutoConfiguration {
    @Bean
    @ConditionalOnMissingBean({HKVStoreService.class})
    public HKVStoreService hkvStore() {
        return new FileSystemHKVStore("./fsStore.conf");
    }

    @Bean
    @ConditionalOnMissingBean({VcStoreService.class})
    public VcStoreService vcStoreService(HKVStoreService hkvStore) {
        return new AtlasHKVVcStoreService(hkvStore);
    }

    @Bean
    @ConditionalOnMissingBean({AtlasAuditor.class})
    public AtlasAuditor auditor() {
        return new AtlasAuditor();
    }

    @Bean(name = "signatureVerificationPolicy")
    public AtlasSignaturePolicy getSignatureVerificationPolicy(
            WaltIdJsonLdCredentialService jsonLdCredentialService,
            WaltIdJwtCredentialService jwtCredentialService
    ) {
        return new AtlasSignaturePolicy(jsonLdCredentialService, jwtCredentialService);
    }

    @Bean(name = "credentialRoleVerificationPolicy")
    @ConditionalOnMissingBean({GxCredentialRoleVerificationPolicy.class})
    public GxCredentialRoleVerificationPolicy credentialRoleVerificationPolicy() {
        return new GxCredentialRoleVerificationPolicy(new ArrayList<>());
    }

    @Bean
    @ConditionalOnMissingBean({AtlasSelfDescriptionSubjectBuilder.class})
    public AtlasSelfDescriptionSubjectBuilder selfDescriptionSubjectBuilder(ObjectMapper objectMapper) {
        return new AtlasSelfDescriptionSubjectBuilder(objectMapper);
    }

    @Bean
    @ConditionalOnMissingBean({AtlasDidResolverFactory.class})
    public AtlasDidResolverFactory didResolverFactory(AtlasKeyService keyService) {
        return new AtlasDidResolverFactory(
                WaltIdServices.INSTANCE.getHttpNoAuth(),
                keyService);
    }

    @Bean
    @ConditionalOnMissingBean({AtlasCryptoService.class})
    public AtlasCryptoService atlasCryptoService(AtlasKeystoreService keystoreService) {
        return new AtlasCryptoService(keystoreService);
    }

    @Bean
    @ConditionalOnMissingBean({AtlasDidService.class})
    public AtlasDidService atlasDidService(
            AtlasCryptoService cryptoService,
            AtlasKeyService atlasKeyService,
            HKVStoreService hkvStore,
            AtlasDidResolverFactory didResolverFactory,
            AtlasKeystoreService keystoreService
    ) {
        return new AtlasDidService(cryptoService, atlasKeyService, hkvStore, didResolverFactory, keystoreService);
    }

    @Bean
    @ConditionalOnMissingBean({AtlasJsonLdCredentialService.class})
    public AtlasJsonLdCredentialService atlasJsonLdCredentialService(
            AtlasKeystoreService keystoreService,
            DidService didService
    ) {
        return new AtlasJsonLdCredentialService(keystoreService, didService);
    }

    @Bean
    @ConditionalOnMissingBean({AtlasJwtCredentialService.class})
    public AtlasJwtCredentialService atlasJwtCredentialService(JwtService jwtService) {
        return new AtlasJwtCredentialService(jwtService);
    }

    @Bean
    @ConditionalOnMissingBean({AtlasJwtService.class})
    public AtlasJwtService atlasJwtService(
            KeyService keyService,
            DidService didService
    ) {
        return new AtlasJwtService(keyService, didService);
    }

    @Bean
    @ConditionalOnMissingBean({AtlasKeyService.class})
    public AtlasKeyService atlasKeyService(
            AtlasKeystoreService keystoreService,
            AtlasCryptoService cryptoService
    ) {
        return new AtlasKeyService(keystoreService, cryptoService);
    }

    @Bean
    @ConditionalOnMissingBean({AtlasKeystoreService.class})
    public AtlasKeystoreService atlasKeystoreService(HKVStoreService hkvStoreService) {
        return new AtlasKeystoreService(hkvStoreService);
    }

    @Bean
    @ConditionalOnMissingBean({AtlasConfiguration.class})
    public AtlasConfiguration atlasConfiguration() {
        return AtlasConfiguration.builder().build();
    }

    @Bean
    @ConditionalOnMissingBean({AtlasSignatoryService.class})
    public AtlasSignatoryService atlasSignatoryService(
            AtlasConfiguration configuration,
            AtlasKeyService atlasKeyService,
            AtlasDidService atlasDidService,
            AtlasVcTemplateService vcTemplateService,
            VcStoreService vcStore,
            JsonLdCredentialService jsonLdCredentialService,
            JwtCredentialService jwtCredentialService
    ) {
        AtlasSignatoryService service = new AtlasSignatoryService(
                configuration,
                atlasKeyService,
                atlasDidService,
                vcTemplateService,
                vcStore,
                jsonLdCredentialService,
                jwtCredentialService
        );
        service.init();
        return service;
    }

    @Bean
    @ConditionalOnMissingBean({AtlasVcTemplateService.class})
    public AtlasVcTemplateService atlasVcTemplateService(HKVStoreService hkvStore) {
        return new AtlasVcTemplateService(hkvStore);
    }

    @Bean
    @ConditionalOnMissingBean({AtlasCustodian.class})
    public AtlasCustodian atlasCustodian(KeyService keyService,
                                         JwtCredentialService jwtCredentialService,
                                         JsonLdCredentialService jsonLdCredentialService,
                                         KeyStoreService keyStore,
                                         VcStoreService vcStore) {
        return new AtlasCustodian(keyService, jwtCredentialService, jsonLdCredentialService, keyStore, vcStore);
    }

    @Bean
    @ConditionalOnMissingBean({AtlasJwtConfigBuilder.class})
    public AtlasJwtConfigBuilder atlasJwtConfigBuilder() {
        return configurer -> {
        };
    }

    @Bean
    @ConditionalOnMissingBean({AtlasJwtAuthorizationConfigurer.class})
    public AtlasJwtAuthorizationConfigurer atlasJwtAuthorizationConfigurer(AtlasJwtConfigBuilder configBuilder) {
        return new AtlasJwtAuthorizationConfigurer(configBuilder);
    }

    @Bean
    @ConditionalOnMissingBean({AtlasJwtInterceptor.class})
    public AtlasJwtInterceptor atlasJwtInterceptor(
            AtlasAuditor auditor,
            @Qualifier("signatureVerificationPolicy") AtlasSignaturePolicy signatureValidator,
            AtlasJwtAuthorizationConfigurer configurer
    ) {
        return new AtlasJwtInterceptor(
                auditor,
                signatureValidator,
                configurer
        );
    }

    @Bean(name = "defaultRestTemplate")
    @SneakyThrows
    public RestTemplate defaultRestTemplate() {
        TrustStrategy acceptingTrustStrategy = (x509Certificates, s) -> true;
        SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
        SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, new NoopHostnameVerifier());
        HttpClientConnectionManager cm = PoolingHttpClientConnectionManagerBuilder.create()
                .setSSLSocketFactory(csf).build();
        HttpClient httpClient = HttpClients.custom().setConnectionManager(cm).build();
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);
        return new RestTemplate(requestFactory);
    }

    @Bean
    @ConditionalOnMissingBean({OpenApiAccessor.class})
    public OpenApiAccessor openApiAccessor(List<GroupedOpenApi> groupedOpenApis,
                                           ObjectFactory<OpenAPIService> defaultOpenAPIBuilder,
                                           AbstractRequestService requestBuilder,
                                           GenericResponseService responseBuilder,
                                           OperationService operationParser,
                                           SpringDocConfigProperties springDocConfigProperties,
                                           SpringDocProviders springDocProviders) {
        return new OpenApiAccessor(groupedOpenApis, defaultOpenAPIBuilder, requestBuilder, responseBuilder, operationParser, springDocConfigProperties, springDocProviders);
    }

    @Bean
    public GroupedOpenApi gx() {
        return GroupedOpenApi.builder()
                .group("gx")
                .packagesToScan("com.cartrust.atlas.springlib")
                .pathsToMatch("/gx/*")
                .build();
    }

    @Bean
    public GroupedOpenApi setup() {
        return GroupedOpenApi.builder()
                .group("setup")
                .packagesToScan("com.cartrust.atlas.springlib")
                .pathsToMatch("/setup/*")
                .build();
    }

    @Bean
    @ConditionalOnMissingBean({AtlasCatalogueConfiguration.class})
    public AtlasCatalogueConfiguration atlasCatalogueConfiguration() {
        return AtlasCatalogueConfiguration.builder()
                .build();
    }

    @Bean
    @ConditionalOnMissingBean({AtlasCatalogue.class})
    public AtlasCatalogue atlasCatalogue(
            AtlasSignatoryService signatoryService,
            AtlasCatalogueConfiguration atlasCatalogueConfiguration,
            @Qualifier("defaultRestTemplate")
            RestTemplate restTemplate,
            ObjectMapper objectMapper,
            Optional<AtlasServiceOfferingManager> atlasServiceOfferingManager) {
        return new AtlasCatalogue(signatoryService, atlasCatalogueConfiguration, restTemplate, objectMapper, atlasServiceOfferingManager);
    }
}
