package com.cartrust.atlas.ssikit.config;

public interface AtlasJwtConfigBuilder {
    void configure(AtlasJwtAuthorizationConfigurer configurer);
}
