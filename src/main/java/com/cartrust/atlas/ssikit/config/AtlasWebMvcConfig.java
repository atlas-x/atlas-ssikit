package com.cartrust.atlas.ssikit.config;

import com.cartrust.atlas.ssikit.AtlasJwtInterceptor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Component
public class AtlasWebMvcConfig implements WebMvcConfigurer {
    private final AtlasJwtInterceptor atlasJwtInterceptor;

    public AtlasWebMvcConfig(AtlasJwtInterceptor atlasJwtInterceptor) {
        this.atlasJwtInterceptor = atlasJwtInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(atlasJwtInterceptor);
    }
}
