package com.cartrust.atlas.ssikit;

import com.cartrust.atlas.ssikit.waltid.AtlasSignatoryService;
import id.walt.auditor.*;
import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.credentials.w3c.VerifiablePresentation;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Slf4j
public class AtlasAuditor {
    public VerificationResult verify(String vcJson, List<VerificationPolicy> policies) {
        VerifiableCredential verifiableCredential = AtlasSignatoryService.toVerifiableCredential(vcJson);
        return this.verify(verifiableCredential, policies);
    }

    public VerificationResult verify(VerifiableCredential vc, List<VerificationPolicy> policies) {
        Map<String, VerificationPolicyResult> verificationPolicyResultMap =
                policies.stream().collect(Collectors.toMap(VerificationPolicy::getId, policy -> {
                    log.debug("Verifying vc with {} ...", policy.getId());
                    VerificationPolicyResult vcResult = policy.verify(vc);
                    AtomicBoolean success = new AtomicBoolean(vcResult.isSuccess());
                    ArrayList<Throwable> allErrors = new ArrayList<>(vcResult.getErrors());
                    if (allErrors.isEmpty() && vc instanceof VerifiablePresentation) {
                        Optional.ofNullable(((VerifiablePresentation) vc).getVerifiableCredential())
                                .ifPresent(vp -> vp.forEach(cred -> {
                                    List<String> type = cred.getType();
                                    String lastTYpe = type.size() > 0 ? type.get(type.size() - 1) : null;
                                    log.debug("Verifying {} in VP with {}...", lastTYpe, policy.getId());
                                    VerificationPolicyResult vpResult = policy.verify(cred);
                                    allErrors.addAll(vpResult.getErrors());
                                    success.compareAndSet(true, vpResult.isSuccess());
                                }));
                    }
                    allErrors.forEach(error -> log.error("{}: {}", policy.getId(), error.getMessage()));
                    return VerificationPolicyResult.Companion.by(success.get(), allErrors);
                }));
        return new VerificationResult(allAccepted(verificationPolicyResultMap), verificationPolicyResultMap);
    }

    private boolean allAccepted(Map<String, VerificationPolicyResult> verificationPolicyResultMap) {
        return verificationPolicyResultMap.values().stream().allMatch(VerificationPolicyResult::isSuccess);
    }
}
