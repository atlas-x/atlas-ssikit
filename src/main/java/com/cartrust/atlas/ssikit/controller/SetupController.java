package com.cartrust.atlas.ssikit.controller;


import com.cartrust.atlas.ssikit.config.AtlasConfiguration;
import com.cartrust.atlas.ssikit.config.AtlasScheduledConfiguration;
import com.cartrust.atlas.ssikit.waltid.AtlasDidService;
import com.cartrust.atlas.ssikit.waltid.AtlasSignatoryService;
import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.model.Did;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@RestController
@RequestMapping("/setup")
public class SetupController {
    private final AtlasScheduledConfiguration scheduledConfiguration;
    private final AtlasConfiguration configuration;
    private final AtlasSignatoryService signatoryService;
    private final AtlasDidService didService;

    public SetupController(AtlasScheduledConfiguration scheduledConfiguration,
                           AtlasConfiguration configuration,
                           AtlasSignatoryService signatoryService,
                           AtlasDidService didService) {
        this.scheduledConfiguration = scheduledConfiguration;
        this.configuration = configuration;
        this.signatoryService = signatoryService;
        this.didService = didService;
    }

    @PostMapping("/identity")
    public ResponseEntity<String> setupIdentity() {
        try {
            signatoryService.init();
            Did didDocument = didService.load(me());
            if (didDocument == null) {
                return ResponseEntity.notFound().build();
            }
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(didDocument.encode());
        } catch (Exception e) {
            return ResponseEntity.internalServerError().body(e.getMessage());
        }
    }

    @PostMapping("/sd")
    public ResponseEntity<String> setupSelfDescription() {
        try {
            scheduledConfiguration.startScheduledTask();
            Optional<VerifiableCredential> credential = Optional
                    .ofNullable(signatoryService.getVcStore().getCredential(
                            configuration.getSelfDescriptionVcId(),
                            configuration.getVcGroup()));
            ResponseEntity.BodyBuilder bodyBuilder = ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON);
            return credential.map(c -> bodyBuilder.body(c.encode())).orElse(bodyBuilder.body("{}"));
        } catch (Exception e) {
            return ResponseEntity.internalServerError().contentType(MediaType.APPLICATION_JSON).body("{\"error\":\"" + e.getMessage() + "\"}");
        }
    }

    private String me() {

        String subjectId;
        try {
            subjectId = signatoryService.getIssuerDid().get(100, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            throw new RuntimeException(e);
        }
        return subjectId;
    }
}
