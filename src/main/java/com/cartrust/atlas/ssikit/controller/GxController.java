package com.cartrust.atlas.ssikit.controller;

import com.cartrust.atlas.ssikit.catalogue.AtlasCatalogue;
import com.cartrust.atlas.ssikit.config.AtlasConfiguration;
import com.cartrust.atlas.ssikit.config.ProofConfigBuilder;
import com.cartrust.atlas.ssikit.gx.AtlasServiceOfferingManager;
import com.cartrust.atlas.ssikit.waltid.AtlasDidService;
import com.cartrust.atlas.ssikit.waltid.AtlasSignatoryService;
import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.credentials.w3c.builder.W3CCredentialBuilder;
import id.walt.credentials.w3c.templates.VcTemplate;
import id.walt.model.Did;
import id.walt.signatory.ProofConfig;
import id.walt.signatory.ProofType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Slf4j
@RestController
@RequestMapping("/gx")
public class GxController {
    private final AtlasConfiguration configuration;
    private final AtlasDidService didService;
    private final AtlasSignatoryService signatoryService;
    private final Optional<AtlasServiceOfferingManager> atlasServiceOfferingManager;
    private final Optional<AtlasCatalogue> atlasCatalogue;

    public GxController(
            AtlasConfiguration configuration,
            AtlasDidService didService,
            AtlasSignatoryService signatoryService,
            Optional<AtlasServiceOfferingManager> atlasServiceOfferingManager,
            Optional<AtlasCatalogue> atlasCatalogue) {
        this.configuration = configuration;
        this.didService = didService;
        this.signatoryService = signatoryService;
        this.atlasServiceOfferingManager = atlasServiceOfferingManager;
        this.atlasCatalogue = atlasCatalogue;
    }


    @GetMapping(path = "/did")
    public @ResponseBody ResponseEntity<String> getDidDocument() {
        try {
            Did didDocument = didService.load(me());
            if (didDocument == null) {
                return ResponseEntity.notFound().build();
            }
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(didDocument.encode());
        } catch (Exception e) {
            return ResponseEntity.internalServerError().body(e.getMessage());
        }
    }

    @GetMapping(path = "/did/{id}")
    public @ResponseBody ResponseEntity<String> getDidDocument(@PathVariable String id) {
        try {
            Did didDocument = didService.load(me());
            if (didDocument == null || !didDocument.getId().endsWith(":" + id)) {
                return ResponseEntity.notFound().build();
            }
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(didDocument.encode());
        } catch (Exception e) {
            return ResponseEntity.internalServerError().body(e.getMessage());
        }
    }

    @GetMapping(path = "/sd")
    public @ResponseBody ResponseEntity<String> getSelfDescription() {
        try {
            Optional<VerifiableCredential> credential = Optional
                    .ofNullable(signatoryService.getVcStore().getCredential(
                            configuration.getSelfDescriptionVcId(),
                            configuration.getVcGroup()));

            if (credential.isEmpty() && !configuration.isReturnEmptySelfDescription()) {
                return ResponseEntity.ofNullable(null);
            }

            VerifiableCredential selfDescription = credential.orElseGet(this::getEmpty);
            String sd = selfDescription.encode();
            ResponseEntity.BodyBuilder rb = ResponseEntity.status(200);
            if (selfDescription.getSdJwt() == null) {
                rb.contentType(MediaType.TEXT_PLAIN);
            } else {
                rb.contentType(MediaType.APPLICATION_JSON);
            }
            return rb.body(sd);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.status(500).contentType(MediaType.TEXT_PLAIN).body(e.getMessage());
        }
    }

    @GetMapping("/so")
    public Map<String, Object> getServiceOfferings(
            @RequestHeader(value = "Authorization", required = false) String auth,
            @RequestParam (value =  "validOnly", required = false, defaultValue = "true") boolean validOnly
    ) {
        return atlasServiceOfferingManager.map(m -> m.listJsonPresentation(validOnly))
                .orElse(new HashMap<>());
    }

    @GetMapping("/so/{offer}")
    public ResponseEntity<?> getServiceOffering(
            @PathVariable("offer") String offer,
            @RequestHeader(value = "Authorization", required = false) String auth
    ) {
        return ResponseEntity.of(atlasServiceOfferingManager.map(m -> m.listJsonPresentation(false).get(offer)));
    }

    @GetMapping("/catalogue")
    public ResponseEntity<?> getCatalogue() {
        if (atlasCatalogue.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        AtlasCatalogue catalogue = atlasCatalogue.get();
        catalogue.init();
        return ResponseEntity.of(Optional.ofNullable(catalogue.present()));
    }

    private String me() {

        String subjectId;
        try {
            subjectId = signatoryService.getIssuerDid().get(100, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            throw new RuntimeException(e);
        }
        return subjectId;
    }

    private VerifiableCredential getEmpty() {
        String subjectId = me();
        VcTemplate gaiaxCredentialSD = signatoryService.getTemplateService().getTemplate(
                "GaiaxCredentialSD",
                true,
                configuration.getTemplatesFolder()
        );

        W3CCredentialBuilder w3CCredentialBuilder = W3CCredentialBuilder.Companion.fromPartial(gaiaxCredentialSD.getTemplate());
        w3CCredentialBuilder.setSubjectId(subjectId);
        w3CCredentialBuilder.setId(subjectId);

        if (configuration.isSelfSignSelfDescription()) {
            ProofConfig proofConfig = createProofConfig(subjectId).withCredentialId(subjectId).withProofType(ProofType.LD_PROOF).build();
            String issued = signatoryService.issue(w3CCredentialBuilder, proofConfig, null, false);
            return VerifiableCredential.Companion.fromString(issued);
        }

        return w3CCredentialBuilder.build();
    }

    public ProofConfigBuilder createProofConfig(String issuerDid) {
        Instant expiration = Instant.now().plus(30, ChronoUnit.MINUTES);
        return ProofConfigBuilder.create(issuerDid)
                .withCredentialId("urn:uuid:" + UUID.randomUUID())
                .withSubjectDid(issuerDid)
                .withProofType(ProofType.JWT)
                .withExpirationDate(expiration);
    }
}
