package com.cartrust.atlas.ssikit.gx;

import com.cartrust.atlas.ssikit.config.AtlasConfiguration;
import com.cartrust.atlas.ssikit.config.ProofConfigBuilder;
import com.cartrust.atlas.ssikit.waltid.AtlasSignatoryService;
import id.walt.auditor.SimpleVerificationPolicy;
import id.walt.auditor.VerificationPolicy;
import id.walt.auditor.VerificationPolicyResult;
import id.walt.auditor.policies.ExpirationDateAfterPolicy;
import id.walt.auditor.policies.IssuedDateBeforePolicy;
import id.walt.auditor.policies.ValidFromBeforePolicy;
import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.credentials.w3c.builder.W3CCredentialBuilder;
import id.walt.credentials.w3c.templates.VcTemplate;
import id.walt.signatory.ProofType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Slf4j
public class AtlasSelfDescriptionManager {
    private final AtlasConfiguration configuration;
    private final AtlasSignatoryService signatoryService;
    private final VerificationPolicy signatureValidator;
    private final AtlasSelfDescriptionSubjectBuilder selfDescriptionSubjectBuilder;
    private final RestTemplate restTemplate;

    public AtlasSelfDescriptionManager(
            AtlasConfiguration configuration,
            AtlasSignatoryService signatoryService,
            VerificationPolicy signatureValidator,
            AtlasSelfDescriptionSubjectBuilder selfDescriptionSubjectBuilder,
            @Qualifier("defaultRestTemplate")
            RestTemplate restTemplate
    ) {
        this.configuration = configuration;
        this.signatoryService = signatoryService;
        this.signatureValidator = signatureValidator;
        this.selfDescriptionSubjectBuilder = selfDescriptionSubjectBuilder;
        this.restTemplate = restTemplate;
    }

    public void check() {
        log.debug("check SD");
        if (!signatoryService.isInitialized()) {
            log.info("Signatory not initialized - please invoke setup identity api");
            return;
        }
        if (hasStoredSelfDescription()) {
            log.debug("there is a stored SD");
            if (isSelfDescriptionSigned()) {
                log.debug("is signed");
                if (isSelfDescriptionSignatureIsValid() && isSelfDescriptionTimesAreValid()) {
                    log.debug("all ok");
                    // do nothing - everything is fine
                    return;
                }
            }
//            else {
//                log.debug("fetch issued");
//                fetchIssuedAndSave();
//                // done - will check status next invocation
//                return;
//            }
        }
        if(fetchIssuedAndSave()) {
            return;
        }
        log.debug("send for issuing");
        sendEmptyAndSaveForIssuing();
    }

    private void sendEmptyAndSaveForIssuing() {
        try {
            signatoryService.getVcStore().deleteCredential(configuration.getSelfDescriptionVcId(), configuration.getVcGroup());
        } catch (Exception e) {
            log.debug("Failed do delete stored credential: {}", e.getMessage());
        }
        VerifiableCredential empty = getEmpty();
        if (sendForIssuing(empty)) {
            save(empty);
        }
    }

    private boolean sendForIssuing(VerifiableCredential verifiableCredential) {

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.ALL));
        headers.setContentType(MediaType.APPLICATION_JSON);
        String json = verifiableCredential.toJson();
        URI uri = URI.create(configuration.getAuthorityVcAddress());
        HttpEntity<String> httpEntity = new HttpEntity<>(json, headers);
        try {
            ResponseEntity<String> exchange = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, String.class);
            if (exchange.getStatusCode().is2xxSuccessful()) {
                return true;
            } else  {
                log.error("Failed to register SD VC");
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return false;
    }

    private void save(VerifiableCredential verifiableCredential) {
        signatoryService.getVcStore().storeCredential(configuration.getSelfDescriptionVcId(), verifiableCredential, configuration.getVcGroup());
    }

    private boolean fetchIssuedAndSave() {
        log.debug("Fetch self description");
        String did = me();
        String templatesFolder = configuration.getTemplatesFolder();
        VcTemplate template = signatoryService.getTemplateService().getTemplate("Access", true, templatesFolder);
        VerifiableCredential vc = issue(template.getTemplate(), did);
        String jwt = vc.encode();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.ALL));
        headers.setBearerAuth(jwt);
        String uri = UriComponentsBuilder.fromHttpUrl(configuration.getAuthorityVcAddress())
                .queryParam("vcId", "{vcId}")
                .encode()
                .toUriString();
        HttpEntity<String> httpEntity = new HttpEntity<>(headers);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("vcId", did);
        try {
            ResponseEntity<String> exchange = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, String.class, parameters);
            if (exchange.getStatusCode().is2xxSuccessful()) {
                String body = exchange.getBody();

                VerifiableCredential verifiableCredential = VerifiableCredential.Companion.fromString(body);

                signatoryService.getVcStore().storeCredential(configuration.getSelfDescriptionVcId(), verifiableCredential, configuration.getVcGroup());
                return true;
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return false;
    }

    private boolean hasStoredSelfDescription() {
        try {
            return signatoryService.getVcStore().getCredential(configuration.getSelfDescriptionVcId(), configuration.getVcGroup()) != null;
        } catch (Exception e) {
            log.error("Got exception while loading SD credential: {}", e.getMessage());
            return false;
        }
    }

    private boolean isSelfDescriptionSigned() {
        try {
            VerifiableCredential credential = signatoryService.getVcStore().getCredential(configuration.getSelfDescriptionVcId(), configuration.getVcGroup());
            return credential != null && (credential.getSdJwt() != null || credential.getProof() != null);
        } catch (Exception e) {
            log.error("Got exception while getting SD credential signed status: {}", e.getMessage());
        }
        return false;
    }

    private boolean isSelfDescriptionSignatureIsValid() {
        try {
            VerifiableCredential credential = signatoryService.getVcStore().getCredential(configuration.getSelfDescriptionVcId(), configuration.getVcGroup());
            VerificationPolicyResult policyResult = signatureValidator.verify(credential);
            if (!policyResult.isSuccess()) {
                log.error("SD signature not valid: {}", policyResult);
            }
            return policyResult.isSuccess();
        } catch (Exception e) {
            log.error("Got exception while getting SD credential signed status: {}", e.getMessage());
        }
        return false;
    }

    private boolean isSelfDescriptionTimesAreValid() {
        try {
            VerifiableCredential credential = signatoryService.getVcStore().getCredential(configuration.getSelfDescriptionVcId(), configuration.getVcGroup());
            List<SimpleVerificationPolicy> policies = List.of(new IssuedDateBeforePolicy(),
                    new ValidFromBeforePolicy(),
                    new ExpirationDateAfterPolicy());

            return policies.stream().filter(p -> !p.verify(credential).isSuccess()).map(p -> {
                log.error("SD times validation failed: {}", p.verify(credential));
                return false;
            }).findFirst().orElse(true);
        } catch (Exception e) {
            log.error("Got exception while getting SD credential signed status: {}", e.getMessage());
        }
        return false;
    }

    private VerifiableCredential getEmpty() {
        String subjectId = me();
        VcTemplate sdCredential = signatoryService.getTemplateService().getTemplate(
                configuration.getSelfDescriptionTemplate(),
                true,
                configuration.getTemplatesFolder()
        );

        W3CCredentialBuilder w3CCredentialBuilder = W3CCredentialBuilder.Companion.fromPartial(sdCredential.getTemplate());
        w3CCredentialBuilder.setSubjectId(subjectId);
        w3CCredentialBuilder.setId(subjectId);

        selfDescriptionSubjectBuilder.build(data -> w3CCredentialBuilder.setFromJson(data));

        return w3CCredentialBuilder.build();
    }

    private VerifiableCredential issue(VerifiableCredential partial, String subjectId) {
        W3CCredentialBuilder w3CCredentialBuilder = W3CCredentialBuilder.Companion.fromPartial(partial);
        w3CCredentialBuilder.setSubjectId(subjectId);

        String issuerDid = me();

        String vc = signatoryService.issue(
                w3CCredentialBuilder,
                createProofConfig(issuerDid).withSubjectDid(subjectId).withProofType(ProofType.JWT).build(),
                null,
                false
        );
        return VerifiableCredential.Companion.fromString(vc);
    }

    private ProofConfigBuilder createProofConfig(String issuerDid) {
        Instant expiration = Instant.now().plus(30, ChronoUnit.DAYS);
        return ProofConfigBuilder.create(issuerDid)
                .withCredentialId("urn:uuid:" + UUID.randomUUID())
                .withSubjectDid(issuerDid)
                .withProofType(ProofType.JWT)
                .withExpirationDate(expiration);
    }

    private String me() {
        String subjectId;
        try {
            subjectId = signatoryService.getIssuerDid().get(100, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            throw new RuntimeException(e);
        }
        return subjectId;
    }
}
