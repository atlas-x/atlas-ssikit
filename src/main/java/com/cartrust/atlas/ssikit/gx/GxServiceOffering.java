package com.cartrust.atlas.ssikit.gx;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GxServiceOffering {

    @JsonProperty("gx-service-offering:providedBy")
    String providedBy;

    @JsonProperty("gx-service-offering:dataProtectionRegime")
    List<String> dataProtectionRegime;

    @JsonProperty("gx-service-offering:description")
    String description;

    @JsonProperty("gx-service-offering:name")
    String name;

    @JsonProperty("gx-service-offering:webAddress")
    String webAddress;

    @JsonProperty("gx-service-offering:api")
    Object api;
}
