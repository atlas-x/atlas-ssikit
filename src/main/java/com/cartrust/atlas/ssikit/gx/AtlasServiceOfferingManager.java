package com.cartrust.atlas.ssikit.gx;

import com.cartrust.atlas.ssikit.config.AtlasConfiguration;
import com.cartrust.atlas.ssikit.config.OpenApiAccessor;
import com.cartrust.atlas.ssikit.config.ProofConfigBuilder;
import com.cartrust.atlas.ssikit.waltid.AtlasSignatoryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.credentials.w3c.builder.W3CCredentialBuilder;
import id.walt.credentials.w3c.templates.VcTemplate;
import id.walt.signatory.ProofType;
import lombok.SneakyThrows;

import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;


public class AtlasServiceOfferingManager {
    private final AtlasConfiguration configuration;
    private final AtlasSignatoryService signatoryService;
    private final OpenApiAccessor openApiAccessor;
    private final ObjectMapper mapper;

    public AtlasServiceOfferingManager(
            AtlasConfiguration configuration,
            AtlasSignatoryService signatoryService,
            OpenApiAccessor openApiAccessor, ObjectMapper mapper) {
        this.configuration = configuration;
        this.signatoryService = signatoryService;
        this.openApiAccessor = openApiAccessor;
        this.mapper = mapper;
    }

    //    @PostConstruct
    public Map<String, VerifiableCredential> getAll() {
        Map<String, VcTemplate> templates = signatoryService.getTemplateService()
                .listTemplates(configuration.getTemplatesFolder())
                .stream()
                .map(VcTemplate::getName)
                .filter(n -> n.toLowerCase().startsWith("ServiceOffering".toLowerCase()))
                .collect(Collectors.toMap(n -> n, this::getTemplate));

        return templates.keySet().stream()
                .filter(name -> templates.get(name).getTemplate() != null)
                .collect(Collectors.toMap(name -> name, name -> prepare(templates.get(name))));
    }

    public Map<String, Object> listJsonPresentation(boolean validOnly) {
        Map<String, VerifiableCredential> all = getAll();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'kk:mm:ss'Z'");

        return all.entrySet()
                .stream()
                .filter(it ->
                        validOnly
                                ? LocalDate.parse(it.getValue().getExpirationDate(), formatter).isAfter(LocalDate.now())
                                : true)
                .map(e -> new Touple(e.getKey(), this.mapCatching(e.getValue().encode())))
                .filter(t -> t.value().isPresent())
                .collect(Collectors.toMap(t -> t.key, t -> t.value.get()));
    }

        public String getCredentialSubject(String name) {
        return configuration.getServiceBaseAddress() +
                configuration.getApiDocsBasePath() +
                "/" +
                name;
    }

    public String getWebAddress(String name) {
        return configuration.getServiceBaseAddress();
    }

    private VcTemplate getTemplate(String name) {
        return signatoryService.getTemplateService().getTemplate(name, true, configuration.getTemplatesFolder());
    }

    VerifiableCredential prepare(VcTemplate template) {
        String name = template.getName();
        String issuer = me();
        String credentialId = configuration.getServiceBaseAddress() + "/gx/so/" + name;
        GxServiceOffering.GxServiceOfferingBuilder offeringBuilder = GxServiceOffering.builder()
                .providedBy(issuer)
                .webAddress(getWebAddress(name));
        getApiDefinition(name).ifPresent(offeringBuilder::api);
        GxServiceOffering offering = offeringBuilder.build();

        String subject = getCredentialSubject(name);
        createProofConfig(issuer);
        W3CCredentialBuilder w3CCredentialBuilder = W3CCredentialBuilder.Companion.fromPartial(Objects.requireNonNull(template.getTemplate()));
        w3CCredentialBuilder.setSubjectId(subject);
        w3CCredentialBuilder.setFromJson(this.toJson(offering));
        String vc = signatoryService.issue(
                w3CCredentialBuilder,
                createProofConfig(issuer).withCredentialId(credentialId).withSubjectDid(subject).withProofType(ProofType.LD_PROOF).build(),
                null,
                false
        );
        return VerifiableCredential.Companion.fromString(vc);
    }

    private Optional<Object> getApiDefinition(String name) {
        try {
            String jsonDefinition = openApiAccessor.getFor(name);
            return mapCatching(jsonDefinition);
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    private String me() {
        String me;
        try {
            me = signatoryService.getIssuerDid().get(100, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            throw new RuntimeException(e);
        }
        return me;
    }

    @SneakyThrows
    private String toJson(GxServiceOffering o) {
        Map<String, Object> body = new HashMap<>();
        body.put("credentialSubject", o);
        return mapper.writeValueAsString(body);
    }

    private Optional<Object> mapCatching(String s) {
        try {
            return Optional.ofNullable(mapper.readValue(s, Object.class));
        } catch (JsonProcessingException e) {
            return Optional.empty();
        }
    }

    private ProofConfigBuilder createProofConfig(String issuerDid) {
        Instant expiration = Instant.now().plus(30, ChronoUnit.DAYS);
        return ProofConfigBuilder.create(issuerDid)
                .withCredentialId("urn:uuid:" + UUID.randomUUID())
                .withSubjectDid(issuerDid)
                .withProofType(ProofType.JWT)
                .withExpirationDate(expiration);
    }

    private record Touple(String key, Optional<Object> value) { }

}
